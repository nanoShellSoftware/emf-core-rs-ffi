# emf-core-rs-ffi

Low-level Rust wrapper of the `emf-core` interface.

## Links

- [`git:emf-core`](https://gitlab.com/nanoShellSoftware/emf-c) Specification.
- [`doc:emf-core`](https://nanoshellsoftware.gitlab.io/emf-c/) Documentation.
- [`git:emf-core-rs-ffi`](https://gitlab.com/nanoShellSoftware/emf-core-rs-ffi) Repository.