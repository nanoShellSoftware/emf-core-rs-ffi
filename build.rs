extern crate bindgen;
extern crate cmake;

fn main() {
    let emf_core_path = "emf-core";
    let out_path = std::path::PathBuf::from(std::env::var("OUT_DIR").unwrap());

    println!("cargo:rerun-if-changed=wrapper.h");

    let emf_core_c_build = cmake::Config::new(emf_core_path)
        .define("EMF_BUILD_DOCS", "OFF")
        .define("EMF_BUILD_BINDINGS", "OFF")
        .define("EMF_BUILD_CORE_CPP", "OFF")
        .define("EMF_ENABLE_CLANG_TIDY", "OFF")
        .build();

    let builder = bindgen::Builder::default()
        .header("wrapper.h")
        .derive_copy(true)
        .derive_debug(true)
        .derive_default(true)
        .derive_eq(true)
        .enable_function_attribute_detection()
        .prepend_enum_name(false)
        .size_t_is_usize(true)
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .clang_arg(format!("-I{}/include", emf_core_c_build.display()))
        .generate()
        .expect("Unable to generate bindings");

    builder
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
