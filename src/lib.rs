//! The low-level Rust interface of `emf-core`.
//!
//! # Note
//!
//! This crate is not intended to be used independently, as it only provides declarations.
//! Incorrect usage of this crate will lead to linker errors.
//! Instead see [`emf-core-rs-ffi-bindings`](https://gitlab.com/nanoShellSoftware/emf-core-rs-ffi-bindings) for a crate, that also implements the bindings.
//!
//! # Uses
//!
//! This crate is useful for implementing the `emf-core` interface.
//!
//! # Resources
//!
//! See [`git:emf-core`](https://gitlab.com/nanoShellSoftware/emf-c) for the specification of the interface. <br>
//! See [`doc:emf-core`](https://nanoshellsoftware.gitlab.io/emf-c/) for the documentation of the interface. <br>
#![allow(missing_debug_implementations)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

/// Panics with an error.
///
/// This allows a program to terminate immediately and provide feedback to the caller of the program.
/// `emf_panic_rs` should be used when a program reaches an unrecoverable state.
/// See [`emf_panic()`](./bindings/core/fn.emf_panic.html) for more information.
///
/// # Examples
/// ```should_panic
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// emf_panic_rs!();
/// emf_panic_rs!("Example error!");
/// emf_panic_rs!("This is a {} {message}", "fancy", message = "message");
/// ```
#[macro_export]
macro_rules! emf_panic_rs {
    () => {
        if cfg!(any(test, doctest)) {
            panic!();
        } else {
            #[allow(unused_unsafe)]
            unsafe {
                emf_panic(std::ptr::null());
            }
        }
    };
    ($msg:expr) => {
        if cfg!(any(test, doctest)) {
            panic!($msg);
        } else {
            let msg = concat!($msg, "\0");
            #[allow(unused_unsafe)]
            unsafe {
                let c_msg = std::ffi::CStr::from_bytes_with_nul_unchecked(msg.as_bytes());
                emf_panic(c_msg.as_ptr());
            }
        }
    };
    ($msg:expr,) => {
        $crate::emf_panic_rs!($msg);
    };
    ($fmt:expr, $($arg:tt)*) => {
        if cfg!(any(test, doctest)) {
            panic!($fmt, $($arg)*);
        } else {
            let mut msg = format!($fmt, $($arg)*);
            msg.push('\0');
            #[allow(unused_unsafe)]
            unsafe {
                let c_msg = std::ffi::CStr::from_bytes_with_nul_unchecked(msg.as_bytes());
                emf_panic(c_msg.as_ptr());
            }
        }
    };
}

/// Asserts that an expression holds.
///
/// Asserts that a boolean expression is `true` at runtime. <br>
/// This will invoke the [`emf_panic_rs!`](./macro.emf_panic_rs.html) macro
/// if the provided expression cannot be evaluated to `true` at runtime.
///
/// # Uses
/// Assertions are always checked in both debug and release builds, and cannot be disabled.
/// See [`emf_debug_assert!`](./macro.emf_debug_assert.html) for assertions
/// that are not enabled in release builds by default.
///
/// # Custom Messages
/// This macro has a second form, where a custom panic message can be provided with
/// or without arguments for formatting.
/// See [`std::fmt`](https://doc.rust-lang.org/std/fmt/index.html) for syntax for this form.
///
/// # Examples
/// ```
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// // The panic message for these assertions is "Condition `$expression` not met."
/// emf_assert!(true);
///
/// fn some_computation() -> bool { true } // a very simple function
///
/// emf_assert!(some_computation());
///
/// // The panic message for these assertions is "Condition `$expression` not met. Error: $msg"
/// let x = true;
/// emf_assert!(x, "x wasn't true!");
///
/// let a = 3; let b = 27;
/// emf_assert!(a + b == 30, "a = {}, b = {}", a, b);
/// ```
#[macro_export]
macro_rules! emf_assert {
    ($cond:expr) => {
        if ($cond) == false {
            $crate::emf_panic_rs!(concat!("Condition `", stringify!($cond), "` not met."));
        }
    };
    ($cond:expr,) => {
        $crate::emf_assert!($cond);
    };
    ($cond:expr, $fmt:expr) => {
        if ($cond) == false {
            $crate::emf_panic_rs!(concat!("Condition `", stringify!($cond), "` not met. Error: ", $fmt));
        }
    };
    ($cond:expr, $fmt:expr, $($arg:tt)*) => {
        if ($cond) == false {
            $crate::emf_panic_rs!(concat!("Condition `", stringify!($cond), "` not met. Error: ", $fmt), $($arg)*);
        }
    };
}

/// Asserts that an expression holds.
///
/// Asserts that a boolean expression is `true` at runtime. <br>
/// This will invoke the [`emf_panic_rs!`](./macro.emf_panic_rs.html) macro
/// if the provided expression cannot be evaluated to `true` at runtime.
///
/// # Uses
/// `emf_debug_assert!` statements are only enabled in non optimized builds by default.
/// An optimized build will not execute `emf_debug_assert!` statements unless
/// `-C debug-assertions` is passed to the compiler.
///
/// # Custom Messages
/// This macro has a second form, where a custom panic message can be provided with
/// or without arguments for formatting.
/// See [`std::fmt`](https://doc.rust-lang.org/std/fmt/index.html) for syntax for this form.
///
/// # Examples
/// ```
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// // The panic message for these assertions is "Condition `$expression` not met."
/// emf_debug_assert!(true);
///
/// fn some_computation() -> bool { true } // a very simple function
///
/// emf_debug_assert!(some_computation());
///
/// // The panic message for these assertions is "Condition `$expression` not met. Error: $msg"
/// let x = true;
/// emf_debug_assert!(x, "x wasn't true!");
///
/// let a = 3; let b = 27;
/// emf_debug_assert!(a + b == 30, "a = {}, b = {}", a, b);
/// ```
#[macro_export]
macro_rules! emf_debug_assert {
    ($cond:expr) => {
        if cfg!(debug_assertions) {
            $crate::emf_assert!($cond);
        }
    };
    ($cond:expr,) => {
        $crate::emf_debug_assert!($cond);
    };
    ($cond:expr, $fmt:expr) => {
        if cfg!(debug_assertions) {
            $crate::emf_assert!($cond, $fmt);
        }
    };
    ($cond:expr, $fmt:expr, $($arg:tt)*) => {
        if cfg!(debug_assertions) {
            $crate::emf_assert!($cond, $fmt, $($arg)*);
        }
    };
}

/// Asserts that two expressions are equal to each other.
///
/// Uses [`emf_assert!`](macro.emf_assert.html) for the assertion.
///
/// # Examples
/// ```
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// let a = 3;
/// let b = 1 + 2;
/// emf_assert_eq!(a, b);
///
/// emf_assert_eq!(a, b, "We are testing addition with {} and {}", a, b);
/// ```
#[macro_export]
macro_rules! emf_assert_eq {
    ($lhs:expr, $rhs:expr) => {
        $crate::emf_assert!($lhs == $rhs);
    };
    ($lhs:expr, $rhs:expr,) => {
        $crate::emf_assert_eq!($lhs, $rhs);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr) => {
        $crate::emf_assert!($lhs == $rhs, $fmt);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr, $($arg:tt)*) => {
        $crate::emf_assert!($lhs == $rhs, $fmt, $($arg)*);
    };
}

/// Asserts that two expressions are equal to each other.
///
/// Uses [`emf_debug_assert!`](macro.emf_debug_assert.html) for the assertion.
///
/// # Examples
/// ```
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// let a = 3;
/// let b = 1 + 2;
/// emf_debug_assert_eq!(a, b);
///
/// emf_debug_assert_eq!(a, b, "We are testing addition with {} and {}", a, b);
/// ```
#[macro_export]
macro_rules! emf_debug_assert_eq {
    ($lhs:expr, $rhs:expr) => {
        $crate::emf_debug_assert!($lhs == $rhs);
    };
    ($lhs:expr, $rhs:expr,) => {
        $crate::emf_debug_assert_eq!($lhs, $rhs);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr) => {
        $crate::emf_debug_assert!($lhs == $rhs, $fmt);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr, $($arg:tt)*) => {
        $crate::emf_debug_assert!($lhs == $rhs, $fmt, $($arg)*);
    };
}

/// Asserts that two expressions are not equal to each other.
///
/// Uses [`emf_assert!`](macro.emf_assert.html) for the assertion.
///
/// # Examples
/// ```
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// let a = 3;
/// let b = 2;
/// emf_assert_ne!(a, b);
///
/// emf_assert_ne!(a, b, "We are testing that the values are not equal");
/// ```
#[macro_export]
macro_rules! emf_assert_ne {
    ($lhs:expr, $rhs:expr) => {
        $crate::emf_assert!($lhs != $rhs);
    };
    ($lhs:expr, $rhs:expr,) => {
        $crate::emf_assert_ne!($lhs, $rhs);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr) => {
        $crate::emf_assert!($lhs != $rhs, $fmt);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr, $($arg:tt)*) => {
        $crate::emf_assert!($lhs != $rhs, $fmt, $($arg)*);
    };
}

/// Asserts that two expressions are not equal to each other.
///
/// Uses [`emf_debug_assert!`](macro.emf_debug_assert.html) for the assertion.
///
/// # Examples
/// ```
/// # use emf_core_rs_ffi::*;
/// # unsafe fn emf_panic(error: *const ::std::os::raw::c_char) -> ! {
/// #    if !error.is_null() {
/// #        let error_len = libc::strlen(error);
/// #        let error_slice =
/// #            std::slice::from_raw_parts(std::mem::transmute::<_, *const u8>(error), error_len);
/// #        let error_msg = std::str::from_utf8(error_slice).unwrap();
/// #        panic!(error_msg);
/// #    } else {
/// #        panic!();
/// #    }
/// # }
/// let a = 3;
/// let b = 2;
/// emf_debug_assert_ne!(a, b);
///
/// emf_debug_assert_ne!(a, b, "We are testing that the values are not equal");
/// ```
#[macro_export]
macro_rules! emf_debug_assert_ne {
    ($lhs:expr, $rhs:expr) => {
        $crate::emf_debug_assert!($lhs != $rhs);
    };
    ($lhs:expr, $rhs:expr,) => {
        $crate::emf_debug_assert_ne!($lhs, $rhs);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr) => {
        $crate::emf_debug_assert!($lhs != $rhs, $fmt);
    };
    ($lhs:expr, $rhs:expr, $fmt:expr, $($arg:tt)*) => {
        $crate::emf_debug_assert!($lhs != $rhs, $fmt, $($arg)*);
    };
}

#[test]
#[should_panic(expected = "This is a fancy message")]
#[cfg(debug_assertions)]
#[allow(unreachable_code)]
fn test_emf_panic_rs() {
    emf_panic_rs!("This is a {} {message}", "fancy", message = "message");
}

#[test]
#[should_panic(expected = "Condition `2 == 3` not met. Error: Expected that 2 is the same as 3")]
#[cfg(debug_assertions)]
fn test_emf_assert() {
    emf_assert!(2 == 3, "Expected that {} is the same as {}", 2, 3);
}

#[test]
#[should_panic(expected = "Condition `2 == 3` not met. Error: Expected that 2 is the same as 3")]
#[cfg(debug_assertions)]
fn test_emf_debug_assert() {
    emf_debug_assert!(2 == 3, "Expected that {} is the same as {}", 2, 3);
}

#[test]
#[should_panic(expected = "Condition `2 == 3` not met. Error: Expected that 2 is the same as 3")]
fn test_emf_assert_eq() {
    emf_assert_eq!(2, 3, "Expected that {} is the same as {}", 2, 3);
}

#[test]
#[should_panic(expected = "Condition `2 == 3` not met. Error: Expected that 2 is the same as 3")]
#[cfg(debug_assertions)]
fn test_emf_debug_assert_eq() {
    emf_debug_assert_eq!(2, 3, "Expected that {} is the same as {}", 2, 3);
}

#[test]
#[should_panic(
    expected = "Condition `3 != 3` not met. Error: Expected that 3 is not the same as 3"
)]
fn test_emf_assert_ne() {
    emf_assert_ne!(3, 3, "Expected that {} is not the same as {}", 3, 3);
}

#[test]
#[should_panic(
    expected = "Condition `3 != 3` not met. Error: Expected that 3 is not the same as 3"
)]
#[cfg(debug_assertions)]
fn test_emf_debug_assert_ne() {
    emf_debug_assert_ne!(3, 3, "Expected that {} is not the same as {}", 3, 3);
}
